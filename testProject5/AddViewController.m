//
//  AddViewController.m
//  testProject5
//
//  Created by USER on 15.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//

#import "AddViewController.h"
#import "DataLoader.h"
#import "Man.h"
@interface AddViewController ()

@end

@implementation AddViewController{
    UILabel *labelName;
    UILabel *labelNumber;
    UILabel *labelAddress;
    DataLoader *_dataLoad;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   self.navigationItem.title = @"Добавление";
    
    UIView *nameContainer = [UIView new];
    nameContainer.translatesAutoresizingMaskIntoConstraints = NO;
    UITextField *textName = [UITextField new];
    textName.translatesAutoresizingMaskIntoConstraints = NO;    textName.text = @"Имя";
    [nameContainer addSubview:textName];
    
    labelName = [UILabel new];
    labelName.translatesAutoresizingMaskIntoConstraints = NO;
    [nameContainer addSubview:labelName];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textName]-[labelName]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(textName, labelName)]];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[textName]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(textName)]];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelName]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(labelName)]];
    
    
    
    UIView *numberContainer = [UIView new];
    numberContainer.translatesAutoresizingMaskIntoConstraints = NO;
    UITextField *textNumber = [UITextField new];
    textNumber.translatesAutoresizingMaskIntoConstraints = NO;    textNumber.text = @"Номер";
    [numberContainer addSubview:textNumber];
    
    labelNumber = [UILabel new];
    labelNumber.translatesAutoresizingMaskIntoConstraints = NO;
    [numberContainer addSubview:labelNumber];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textNumber]-[labelNumber]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(textNumber, labelNumber)]];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[textNumber]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(textNumber)]];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelNumber]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(labelNumber)]];
    
    
    UIView *addressContainer = [UIView new];
    addressContainer.translatesAutoresizingMaskIntoConstraints = NO;
    UITextField *textAddress = [UITextField new];
    textAddress.translatesAutoresizingMaskIntoConstraints = NO;    textAddress.text = @"Адрес";
    [addressContainer addSubview:textAddress];
    
    labelAddress = [UILabel new];
    labelAddress.translatesAutoresizingMaskIntoConstraints = NO;
    [addressContainer addSubview:labelAddress];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textAddres]-[labelAddress]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(textAddress, labelAddress)]];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[textAddres]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(textAddress)]];
    
    [nameContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[labelAddresss]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(labelAddress)]];
    
    
    UIView *buttonContainer = [UIView new];
    buttonContainer.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIButton *addBut = [UIButton new];
    addBut.translatesAutoresizingMaskIntoConstraints = NO;
    addBut.backgroundColor = [UIColor greenColor];
    [addBut setTitle:@"Добавить" forState:UIControlStateNormal];
    [addBut addTarget:self action:@selector(addM:) forControlEvents:UIControlEventTouchUpInside];
    [buttonContainer addSubview:addBut];
    
    UIButton *cancelBut = [UIButton new];
    cancelBut.translatesAutoresizingMaskIntoConstraints = NO;
    cancelBut.backgroundColor = [UIColor greenColor];
    [cancelBut setTitle:@"Отменить" forState:UIControlStateNormal];
    [cancelBut addTarget:self action:@selector(cancelM:) forControlEvents:UIControlEventTouchUpInside];
    [buttonContainer addSubview:addBut];
    
    [buttonContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[addBut]-[cancelBut]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(addBut, cancelBut)]];
    
    [buttonContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[addBut]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(addBut)]];
    
    [buttonContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[cancelBut]-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(cancelBut)]];
    
    
    
    
    [self.view addSubview:nameContainer];
    [self.view addSubview:numberContainer];
    [self.view addSubview:addressContainer];
    [self.view addSubview:buttonContainer];
    
    
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[nameContainer]-|"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:NSDictionaryOfVariableBindings(nameContainer)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[numberContainer]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(numberContainer)]];
    
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[addressContainer]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(addressContainer)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonContainer]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(buttonContainer)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[nameContainer]-[numberContainer]-[addressContainer]-[buttonContainer]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(nameContainer, numberContainer,addressContainer, buttonContainer)]];
    
    


    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_dataLoad getData];
}

#pragma mark - privateMethods

- (void) addM:(id)sender{
    if ((![labelAddress.text  isEqual: @""]) && (![labelName.text  isEqual: @""]) && (![labelNumber.text  isEqual: @""])){
    [_dataLoad setName:labelName.text];
    [_dataLoad setNumber:labelNumber.text];
        [_dataLoad setAddress:labelAddress.text]; } else{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Пожалуйста. Введите все данные!" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Action" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                            }]];
        }
            }

-(void) cancelM:(id)sender{
    //Как уйти с этой view обратно?
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
