//
//  ShowViewController.m
//  testProject5
//
//  Created by USER on 15.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//

#import "ShowViewController.h"
#import "DataLoader.h"
#import "Man.h"

@interface ShowViewController ()

@end

@implementation ShowViewController{
    UITextField *textName;
    UITextField *textNumber;
    UITextField *textAddress;
   // NSArray *_array;
    DataLoader *_dataLoader;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Просмотр";
    
    
    textName = [UITextField new];
    textName.translatesAutoresizingMaskIntoConstraints = NO;
    textName.text = @"Имя: Загрузкка...";
    [self.view addSubview:textName];
    
    

    textNumber = [UITextField new];
    textNumber.translatesAutoresizingMaskIntoConstraints = NO;
    textNumber.text = @"Номер: Загрузка...";
    [self.view addSubview:textNumber];
    
   
    
    textAddress = [UITextField new];
    textAddress.translatesAutoresizingMaskIntoConstraints = NO;
    textAddress.text = @"Адрес: Загрузка...";
    [self.view addSubview:textAddress];
    

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[texteName]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(textName)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textNumber]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(textNumber)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textAddress]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(textAddress)]];
    
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[textName]-[textNumber]-[textAddress]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(textName, textNumber, textAddress)]];
   _dataLoader = [[DataLoader alloc] initWithDelegate:self];  
 
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_dataLoader getData];
}

#pragma mark - DataLoaderProtocol

- (void) setResultLoad:(NSArray*)resultArray{
    NSMutableArray *nameArray = [NSMutableArray new];
    NSMutableArray *numberArray = [NSMutableArray new];
    NSMutableArray *addressArray = [NSMutableArray new];
    
    for (Man *man in resultArray){
        [nameArray addObject:man.name];
        [numberArray addObject:man.number];
        [addressArray addObject:man.address];
    }
    //_array = mArray;
    
    textName.text = [NSString stringWithFormat:@"Имя: %@",nameArray[_indexshow.row]];
    textNumber.text = [NSString stringWithFormat:@"Номер: %@",numberArray[_indexshow.row]];
    textAddress.text = [NSString stringWithFormat:@"Адрес: %@",addressArray[_indexshow.row]];
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
