//
//  ViewController.m
//  testProject5
//
//  Created by Head HandH on 11.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//

#import "ViewController.h"
#import "DataLoader.h"
#import "Man.h"
#import "AddViewController.h"
#import "ShowViewController.h"

@interface ViewController ()<UITableViewDataSource, DataLoaderDelegate>

@end

@implementation ViewController{
    UITableView *_tableView;
    NSArray *_array;
    DataLoader *_dataLoader;
    AddViewController *addViewController;
    ShowViewController *showViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Справочник";
    
    _tableView = [UITableView new];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
   // _tableView.backgroundColor = [UIColor grayColor];
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
        
    
    UIButton *buttonAdd = [UIButton new];
    buttonAdd.translatesAutoresizingMaskIntoConstraints = NO;
    buttonAdd.backgroundColor = [UIColor blueColor];
    [buttonAdd setTitle:@"Добавить" forState:UIControlStateNormal];
    [buttonAdd addTarget:self action:@selector(addMan) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonAdd];
    
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonAdd]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(buttonAdd)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[_tableView]-[buttonAdd(60)]-10-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(buttonAdd, _tableView)]];

    _dataLoader = [[DataLoader alloc] initWithDelegate:self];
    addViewController =[AddViewController new];
    showViewController =[ShowViewController new];    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_dataLoader getData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    if (!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
    }
    cell.textLabel.text = _array[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    showViewController.indexshow = indexPath;
    [self.navigationController pushViewController:showViewController animated:YES];
}



#pragma mark - DataLoaderProtocol

- (void) setResultLoad:(NSArray*)resultArray{
    NSMutableArray *mArray = [NSMutableArray new];
    for (Man *man in resultArray){
        [mArray addObject:man.name];
    }
    _array = mArray;
    [_tableView reloadData];
}

#pragma mark - privateMethods

- (void) addName:(id)sender{
    [_dataLoader setName:@"123"];
}


-(void) addMan{
    
    [self.navigationController pushViewController:addViewController animated:YES];
}
@end
