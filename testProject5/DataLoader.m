//
//  DataLoader.m
//  testProject5
//
//  Created by Head HandH on 11.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//

#import "DataLoader.h"
#import "Man.h"

@implementation DataLoader{
    id<DataLoaderDelegate> _delegate;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil){
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if(_persistentStoreCoordinator != nil){
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]){
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if(_managedObjectContext != nil){
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if(coordinator != nil){
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    if(managedObjectContext != nil) {
        if([managedObjectContext hasChanges] && ![managedObjectContext save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Helpers -

- (NSArray *)fieldsForEntity:(NSString *)entityName exceptFields:(NSArray *)fields
{
    NSSet * sourceSet = [[NSSet alloc] initWithArray:[self fieldsForEntity:entityName]];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"NOT SELF IN %@",fields];
    
    NSSet * result = [sourceSet filteredSetUsingPredicate:predicate];
    
    return [result allObjects];
}

- (NSArray *)fieldsForEntity:(NSString *)entityName
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]];
    NSMutableArray * result = [NSMutableArray arrayWithArray:[[entity attributesByName] allKeys]];
    
    NSDictionary * relations = [entity relationshipsByName];
    [result addObjectsFromArray:[relations allKeys]];
    
    return result;
}

- (instancetype) initWithDelegate:(id<DataLoaderDelegate>)delegate{
    self = [super init];
    if (self){
        _delegate = delegate;
    }
    return self;
}

- (void) getData{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Man" inManagedObjectContext:[self managedObjectContext]];
    [request setEntity:entity];
    NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:nil];
    [_delegate setResultLoad:results];
}

- (void) setName:(NSString*)name{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Man" inManagedObjectContext:[self managedObjectContext]];
    Man *man = [[Man alloc] initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
    man.name = name;
    [self saveContext];
    [self getData];
}


- (void) setNumber:(NSString*)number{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Man" inManagedObjectContext:[self managedObjectContext]];
    Man *man = [[Man alloc] initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
    man.number = number;
    [self saveContext];
    [self getData];
}


- (void) setAddress:(NSString*)address{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Man" inManagedObjectContext:[self managedObjectContext]];
    Man *man = [[Man alloc] initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
    man.address = address;
    [self saveContext];
    [self getData];
}


@end
