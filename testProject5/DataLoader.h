//
//  DataLoader.h
//  testProject5
//
//  Created by Head HandH on 11.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol DataLoaderDelegate <NSObject>

@required

- (void) setResultLoad:(NSArray*)resultArray;

@end

@interface DataLoader : NSObject

@property (readonly, strong, nonatomic,retain) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic,retain) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic,retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

//- (void)saveContext;
//
//- (NSArray *)fieldsForEntity:(NSString *)entityName exceptFields:(NSArray *)fields;

- (instancetype) initWithDelegate:(id<DataLoaderDelegate>)delegate;

- (void) getData;

- (void) setName:(NSString*)name;

- (void) setNumber:(NSString*)number;

- (void) setAddress:(NSString*)address;
@end

