//
//  Man.h
//  testProject5
//
//  Created by Head HandH on 11.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Man : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Man+CoreDataProperties.h"
