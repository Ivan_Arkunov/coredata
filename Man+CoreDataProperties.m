//
//  Man+CoreDataProperties.m
//  testProject5
//
//  Created by Head HandH on 11.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Man+CoreDataProperties.h"

@implementation Man (CoreDataProperties)

@dynamic name;
@dynamic number;
@dynamic address;
@end
