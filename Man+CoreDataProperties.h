//
//  Man+CoreDataProperties.h
//  testProject5
//
//  Created by Head HandH on 11.12.15.
//  Copyright © 2015 HandH. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Man.h"

NS_ASSUME_NONNULL_BEGIN

@interface Man (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *number;
@property (nullable, nonatomic, retain) NSString *address;
@end

NS_ASSUME_NONNULL_END

